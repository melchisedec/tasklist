define( function() {
    
    let $$ = Dom7;

    function start( main ) {
        load();

        $$( document ).on( 'pageBeforeInit', function( event ){
            if ( event.detail.page.name == 'about_item' )
                $$( '.navbar  .left' ).html( '<a href="index.html" class="link icon-only"> <i class="icon icon-back"></i> </a>' );
            else 
                $$( '.navbar .left' ).html( '' );  
        } ); 
    }

    function load() {
        require( [ '../js/init_controller' ], function( controller ) {
            //controller.start();
        } );
    }

    return {
        start: start,
        load: load
    };
} );