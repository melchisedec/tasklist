require.config( {
    paths: {
        handlebars: 'handlebars.js',
        hbs: 'hbs.js',
        text: 'text.js'
    },
    shim: {
        handlebars: {
            exports: 'Handlebars'
        }
    }
} );



define( 'app', [ '../../js/router' ], function( route ) {
   
    //set initialization for the Framework7
    let app = new Framework7( {
        modalTitle: 'Todo List',
        animateNavBackIcon: true
    } );

    let main_view = app.addView( '.view-main', {
        dynamicNavbar: true
    } );
    
    route.start( main_view );

    return {
        app: app,
        main_view: main_view
    };
} );