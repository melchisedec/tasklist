define( [ '../js/init_view', '../js/init_model' ], function( view_setup, model_setup ) {
    //create tables
    model_setup.create_tables();
    //download users
    //model_setup.download_users();
    //download tasks
    model_setup.download_tasks();


} );