define( function(){

    let pagination = {
        per_page : 1,
        page : 1
    },
        api_base = 'http://192.168.56.1:90/', 
        $$ = Dom7,
        app_version = '0.0.1',
        db =  window.openDatabase( 'todoapp', app_version, 'Todo db', 50000 );
    
    function create_tables() {
       db.transaction( function( tx ){
            tx.executeSql( "CREATE table users( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, username CHAR(20), password CHAR(50) )",[] );
            tx.executeSql( "CREATE table user_meta( id INTEGER PRIMARY KEY AUTOINCREMENT,remote_id INTEGER, meta_key CHAR(50), meta_value CHAR(60) )", [] );
            tx.executeSql( "CREATE table post_types( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, singular CHAR(25), plural CHAR(50) )", [] );
            tx.executeSql( "CREATE table posts( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, post_type_id INTEGER, user_id INTEGER, parent_id INTEGER, name CHAR(50) ) ");
            tx.executeSql( "CREATE table post_meta( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, meta_key CHAR(60), meta_value CHAR(75) )", [] );

        } );
    }

    function download_users1() {
        $$.post(
            api_base + 'users',
            {
                per_page: pagination.per_page,
                page: pagination.page
            },
            function( response ) {
                let records = JSON.parse( response ),
                    record = records.users[ 0 ];
                if ( records.result == 'successful' ) {
                    if ( records.users.length == 0 && pagination.page == 1 ) {
    
                        console.log( 'Setup cannot continue. No users were found.' );
                    }
                    if ( records.users.length == 0 && pagination.page > 1 ) {
                        pagination.page = 1;
                        download_tasks();
                        //console.log( pagination.page );
                        //console.log( pagination.per_page );
                        return;
                    }
                    db.transaction( function( tx ) {
                        tx.executeSql( 'SELECT DISTINCT id FROM users WHERE username = ? AND password = ? LIMIT 1', [ record.username, record.password ], function( tx, result ) {
                            if ( result.rows.length == 1 ) {
                                let index = 0;
                                if( index == ( records.users.length - 1 ) ) {
                                    pagination.page = ( records.users.length < pagination.per_page ) ? 1 : pagination.page + 1;
                                    if( pagination.per_page == records.users.length )
                                        download_users();
                                    //else
                                        //download_tasks( pagination.page, pagination.per_page );
                                }
                            }   
                            else {
                                tx.executeSql( 'INSERT INTO users ( remote_id, username, password ) VALUES ( ?, ?, ? )', [ record.id, record.username, record.password ], function( tx, result ) {
                                    let index = 0;
                                   if( index == ( records.users.length - 1 ) ) {
                                       pagination.page = ( records.users.length < pagination.per_page ) ? 1 : pagination.page + 1;
                                       if( pagination.per_page == records.users.length )
                                            download_users();
                                       //else
                                           // download_tasks( pagination.page, pagination.per_page );
                                   }  
                                } );
                            }     
                        } );
                    } );
                }
                else
                    download_users();
            },
            function() {
                download_users();
            }
        );
    }

    function download_tasks1() {
       // console.log( 'page' + pagination.page );
        // console.log( 'per_page' + pagination.page );
        $$.post(
               api_base + 'tasks',
               {
                    page: pagination.page,
                    per_page: pagination.per_page
               },
                /*function( response ) {
                   let task_records = JSON.parse( response ),
                       tasks = task_records.tasks
                   if( task_records.result == 'successful' ) {
                       console.log( tasks );
                        if( tasks.length == 0 && pagination.page == 1 ) {
                            console.log( 'no tasks to work' );
                        }
                        if( tasks.length == 0 && pagination.page > 1 ) {
                            pagination.page = 1;
                            console.log( 'done downloading tasks' );
                            move_on();
                            return;
                        }
                        db.transaction( function( tx ) {
                            tx.executeSql( "SELECT DISTINCT id FROM post_types WHERE singular = ? LIMIT 1", [ task_records.task_category.singular ], function( tx, result ) {
                                
                                let post_type_id =  1;
                                if( result.rows.length == 0 ) {
                                    tx.executeSql( "INSERT INTO post_types( singular, plural ) VALUES( ?, ? )", [ task_records.task_category.singular, task_records.task_category.plural ], function( tx, result ){
                                       let posttypeid = result.insertId;

                                       console.log( 'id' + posttypeid );
                                       console.log(  'l' +tasks.length );
                                       console.log(  'pp' +per_page );
                                       console.log(  'pg' +page );
                                       
                                      

                                    } );
                                }
                                else {
                                    $$.each( tasks, function( index, task ) {
                                       update_posts( post_type_id, task.id, task.name );
                                        if( index == ( tasks.length - 1 ) ) {
                                            pagination.page = ( tasks.length < pagination.per_page ) ? 1 :( pagination.page + 1 );
                                            if( pagination.per_page == tasks.length ) 
                                                download_tasks();
                                            else 
                                                move_on();
                                        }
                                    } );
                                }
                            } );
                        } ); 
                   }
                }*/
                function ( response ) {
                     console.log( response );
                     console.log( pagination.page );
                     //console.log( pagination.per_page );
                }
        );  
    } 
    
    function update_posts( post_type_id,remote_id, name ) {
        post_type_id = ( post_type_id == 'undefined' ) ? 0 : parseInt( post_type_id );
        //user_id =  ( user_id == 'undefined' ) ? 0 : parseInt( user_id );
        //parent_id =  ( parent_id == 'undefined' ) ? 0 : parseInt( parent_id );
        remote_id =  ( remote_id == 'undefined' ) ? 0 : parseInt( remote_id );

        let query = '',
            params = [];
        if( remote_id != 0 ) {
            query += "SELECT DISTINCT id FROM posts WHERE post_type_id = ? AND name = ? AND remote_id = ? LIMIT 1";
            params.push( post_type_id, name, remote_id );
        }
        else {
            query += "SELECT DISTINCT id FROM posts WHERE post_type_id = ? AND name = ? LIMIT 1";
            params.push( post_type_id, name );
        }

        db.transaction( function( tx ) {
            tx.executeSql( query, params, function( tx, result ) {
                console.log( result.rows.length );
                
                if( result.rows.length == 0 ){
                    tx.executeSql( "INSERT INTO posts( remote_id, post_type_id,name ) VALUES( ?, ?, ?)", [ remote_id, post_type_id, name ], function( tx, result ) {
                       console.log( result + ' inserted into posts' );
                    } );
                }
                else {
                    console.log( 'just to update please' );
                } 
            } );
        } );

    
    
    }
    
    function move_on() {
        console.log( 'build' );
    }

    function update_user_meta( user_id, metakey, metavalue ) {
        user_id = typeof user_id != 'undefined' ? parseInt( user_id ) : 0;
        metavalue = typeof metavalue != 'undefined' ? metavalue : '';

        db.transaction( function( tx ) {
            tx.executeSql( "SELECT DISTINCT id FROM user_meta WHERE id = ? LIMIT 1", [ user_id ], function( tx, result ) {
                if( result.rows.length == 0 ) 
                    tx.executeSql( "INSERT INTO user_meta( remote_id, meta_key, meta_value ) VALUES ( ?, ?, ? ) ", [ user_id, metakey, metavalue ] );
                else
                    tx.executeSql( "UPDATE user_meta SET remote_id = ?, meta_key = ?, meta_value = ? WHERE id =  ?", [ user_id, metakey, metavalue ] );
            } );
        } );
    }


    function download_tasks() {
        $$.post(
            api_base + 'tasks',
            {
                per_page: pagination.per_page,
                page: pagination.page
            },
            function( responses ) {
                let response = JSON.parse( responses ),
                    tasklist = response.tasks,
                    category = response.task_category;
            
                if ( response.result == 'successful' ) {
                    if ( tasklist.length == 0 && pagination.page == 1 ) {
                        //app.hidePreloader();
    
                        console.log( 'Setup cannot continue. No modules found.' );
    
                        /*setTimeout( function() {
                            navigator.app.exitApp();
                        }, 5000 ); */
                    }
    
                    else if ( tasklist.length == 0 && pagination.page > 1 ) {
                        pagination.page = 1;
    
                        //download_forms();
                        console.log( 'moving on' );
                    }
    
                    //var taxonomy = response.taxonomy;
                    db.transaction( function( tx ) {
                        tx.executeSql( 'SELECT id FROM post_types WHERE singular = ? LIMIT 1', [ category.singular ], function( tx, result ) {
                            console.log( result.rows.length );
                            if ( result.rows.length == 1 )
                                $$.each( tasklist, function( index, module ) {
                                    update_posts( result.rows.item( 0 ).id, module.id, module.name );
                                    //update_posts( , module.name, module.meta, module.id );
    
                                    if ( index == ( tasklist.length - 1 ) ) {
                                        pagination.page = ( tasklist.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );
    
                                        if ( pagination.per_page == tasklist.length )
                                            download_tasks();
    
                                        else
                                            //download_forms();
                                            console.log( 'moving on' );
                                    }
                                } );
    
                            else
                                tx.executeSql( 'INSERT INTO post_types ( remote_id, singular, plural ) VALUES ( ?, ?, ? )', [ category.id, category.singular, category.plural ], function( tx, result ) {
                                    $$.each( response.modules, function( index, module ) {
                                        //update_term( result.insertId, module.name, module.meta, module.id );
                                        update_posts( result.insertId, module.id, module.name );
    
                                        if ( index == ( tasklist.length - 1 ) ) {
                                            pagination.page = ( tasklist.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );
    
                                            if ( pagination.per_page == tasklist.length )
                                                download_tasks();
    
                                            else
                                                //download_forms();
                                                console.log( 'moving on' );
                                        }
                                    } );
                                } );
                        } );
                    } );
                } 
    
               // else
                    //download_modules();
            },
            function() {
                //download_modules();
            }
        );
    }

    return {
        create_tables: create_tables,
        download_tasks: download_tasks
    }


} );