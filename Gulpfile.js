let gulp = require( 'gulp' );
let sass = require( 'gulp-sass' );

gulp.task( 'style', function() {
    gulp.src( 'www/scss/index.scss' )
        .pipe( sass().on( 'error', sass.logError ) )
        .pipe( gulp.dest( 'www/css/' ) )
} );

gulp.task( 'default', function() {
    gulp.watch( 'www/scss/**/*.scss', [ 'style' ] );
} );